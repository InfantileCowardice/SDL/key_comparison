#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "text.h"

static const int WINDOW_SIZE_X = 500;
static const int WINDOW_SIZE_Y = 200;

static const int FONT_WIDTH  = 13;
static const int FONT_HEIGHT = 20;
static const int FONT_SIZE   = 24;

static const int FONT_OFFSET_X = 20;
static const int FONT_OFFSET_Y = 10;

static const int TEXT_SECOND_COLUMN = FONT_OFFSET_X + FONT_WIDTH * 17;

int main(int argc, char** argv) {
    int exit_code = EXIT_SUCCESS;

    // The path to the TTF font file.
    char* font_path = "";

    // Get the font path from the provided arguments or print the usage message
    // and exit.
    if(argc != 2) {
        printf("Usage: %s FONT_PATH\n", argv[0]);
        goto program_exit;
    }

    font_path = argv[1];

    if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        exit_code = EXIT_FAILURE;
        printf("SDL_Init error: %s\n", SDL_GetError());
        goto init_error;
    }

    if(TTF_Init() != 0) {
        exit_code = EXIT_FAILURE;
        printf("TTF_Init error: %s\n", SDL_GetError());
        goto init_error;
    }

    SDL_Window* window = SDL_CreateWindow(
        "Key comparison",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        WINDOW_SIZE_X, WINDOW_SIZE_Y,
        SDL_WINDOW_SHOWN
    );

    if(!window) {
        exit_code = EXIT_FAILURE;
        printf("SDL_Window error: %s\n", SDL_GetError());
        goto window_creation_error;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(
        window,
        -1,
        0
    );

    if(!renderer) {
        exit_code = EXIT_FAILURE;
        printf("SDL_CreateRenderer error: %s", SDL_GetError());
        goto renderer_creation_error;
    }

    TTF_Font* font = TTF_OpenFont(font_path, FONT_SIZE);

    if(!font) {
        exit_code = EXIT_FAILURE;
        printf("TTF_OpenFont error: %s\n", SDL_GetError());
        goto font_opening_error;
    }

    bool running = true;

    char* keycode  = calloc(20, sizeof(char));
    strcpy(keycode, "NULL");

    char* keycode_int = calloc(20, sizeof(char));
    strcpy(keycode_int, "NULL");

    char* scancode = calloc(20, sizeof(char));
    strcpy(scancode, "NULL");

    while(running) {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);

        int current_line = 0;

        SDL_Event evnt;
        while(SDL_PollEvent(&evnt)) {
            switch(evnt.type) {
            case SDL_QUIT:
                running = false;
                break;

            case SDL_KEYDOWN:
                //if(evnt.key.keysym.sym == SDLK_ESCAPE)
                //    running = false;
                //else
                    extract_info(
                        evnt.key.keysym,
                        keycode,
                        keycode_int,
                        scancode
                    );
                break;

            default:
                break;
            }
        }

        /* render the last keydown event */
        render_text(
            "last keydown:",
            FONT_OFFSET_X,
            FONT_OFFSET_Y + current_line * FONT_HEIGHT,
            font,
            renderer
        );
        render_text(
            keycode,
            FONT_OFFSET_X + TEXT_SECOND_COLUMN,
            FONT_OFFSET_Y + current_line * FONT_HEIGHT,
            font,
            renderer
        );
        ++current_line;

        render_text(
            "keycode int val:",
            FONT_OFFSET_X,
            FONT_OFFSET_Y + current_line * FONT_HEIGHT,
            font,
            renderer
        );
        render_text(
            keycode_int,
            FONT_OFFSET_X + TEXT_SECOND_COLUMN,
            FONT_OFFSET_Y + current_line * FONT_HEIGHT,
            font,
            renderer
        );
        ++current_line;

        render_text(
            "last scancode:",
            FONT_OFFSET_X,
            FONT_OFFSET_Y + current_line * FONT_HEIGHT,
            font,
            renderer
        );
        render_text(
            scancode,
            FONT_OFFSET_X + TEXT_SECOND_COLUMN,
            FONT_OFFSET_Y + current_line * FONT_HEIGHT,
            font,
            renderer
        );

        SDL_RenderPresent(renderer);
    }

    free(scancode);
    free(keycode_int);
    free(keycode);

    TTF_CloseFont(font);
font_opening_error:
    SDL_DestroyRenderer(renderer);
renderer_creation_error:
    SDL_DestroyWindow(window);
window_creation_error:
    TTF_Quit();
    SDL_Quit();
init_error:
program_exit:
    return exit_code;
}
