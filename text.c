#include "text.h"

int render_text(char* message,
                int x,
                int y,
                TTF_Font* font,
                SDL_Renderer* renderer) {
    static const SDL_Color text_color = {.r = 255, .g = 255, .b = 255};

    int ret = 0;

    SDL_Surface* message_surface = TTF_RenderText_Solid(
        font,
        message,
        text_color
    );

    if(!message_surface) {
        ret = -1;
        printf("TTF_RenderText_Solid error: %s\n", SDL_GetError());
        goto surface_error;
    }

    SDL_Texture* message_texture = SDL_CreateTextureFromSurface(
        renderer,
        message_surface
    );

    if(!message_texture) {
        ret = -1;
        printf("SDL_CreateTextureFromSurface error: %s\n", SDL_GetError());
        goto texture_error;
    }

    SDL_Rect message_rect = {
        .x = x,
        .y = y,
        .w = message_surface->w,
        .h = message_surface->h
    };

    SDL_RenderCopy(renderer, message_texture, NULL, &message_rect);

    SDL_DestroyTexture(message_texture);
texture_error:
    SDL_FreeSurface(message_surface);
surface_error:
    return ret;
}

void extract_info(SDL_Keysym key,
                  char* code,
                  char* code_int,
                  char* scancode) {
    strcpy(code, stringfy_sym(key.sym));
    sprintf(code_int, "%d", key.sym);
    sprintf(scancode, "%d", key.scancode);
}

#define CASE(KEY) case KEY: return #KEY; break

static char* stringfy_sym(SDL_Keycode code) {
    switch(code) {
        CASE(SDLK_UNKNOWN);

        CASE(SDLK_RETURN);
        CASE(SDLK_ESCAPE);
        CASE(SDLK_BACKSPACE);
        CASE(SDLK_TAB);
        CASE(SDLK_SPACE);
        CASE(SDLK_EXCLAIM);
        CASE(SDLK_QUOTEDBL);
        CASE(SDLK_HASH);
        CASE(SDLK_PERCENT);
        CASE(SDLK_DOLLAR);
        CASE(SDLK_AMPERSAND);
        CASE(SDLK_QUOTE);
        CASE(SDLK_LEFTPAREN);
        CASE(SDLK_RIGHTPAREN);
        CASE(SDLK_ASTERISK);
        CASE(SDLK_PLUS);
        CASE(SDLK_COMMA);
        CASE(SDLK_MINUS);
        CASE(SDLK_PERIOD);
        CASE(SDLK_SLASH);
        CASE(SDLK_0);
        CASE(SDLK_1);
        CASE(SDLK_2);
        CASE(SDLK_3);
        CASE(SDLK_4);
        CASE(SDLK_5);
        CASE(SDLK_6);
        CASE(SDLK_7);
        CASE(SDLK_8);
        CASE(SDLK_9);
        CASE(SDLK_COLON);
        CASE(SDLK_SEMICOLON);
        CASE(SDLK_LESS);
        CASE(SDLK_EQUALS);
        CASE(SDLK_GREATER);
        CASE(SDLK_QUESTION);
        CASE(SDLK_AT);
        CASE(SDLK_LEFTBRACKET);
        CASE(SDLK_BACKSLASH);
        CASE(SDLK_RIGHTBRACKET);
        CASE(SDLK_CARET);
        CASE(SDLK_UNDERSCORE);
        CASE(SDLK_BACKQUOTE);
        CASE(SDLK_a);
        CASE(SDLK_b);
        CASE(SDLK_c);
        CASE(SDLK_d);
        CASE(SDLK_e);
        CASE(SDLK_f);
        CASE(SDLK_g);
        CASE(SDLK_h);
        CASE(SDLK_i);
        CASE(SDLK_j);
        CASE(SDLK_k);
        CASE(SDLK_l);
        CASE(SDLK_m);
        CASE(SDLK_n);
        CASE(SDLK_o);
        CASE(SDLK_p);
        CASE(SDLK_q);
        CASE(SDLK_r);
        CASE(SDLK_s);
        CASE(SDLK_t);
        CASE(SDLK_u);
        CASE(SDLK_v);
        CASE(SDLK_w);
        CASE(SDLK_x);
        CASE(SDLK_y);
        CASE(SDLK_z);

        CASE(SDLK_CAPSLOCK);

        CASE(SDLK_F1);
        CASE(SDLK_F2);
        CASE(SDLK_F3);
        CASE(SDLK_F4);
        CASE(SDLK_F5);
        CASE(SDLK_F6);
        CASE(SDLK_F7);
        CASE(SDLK_F8);
        CASE(SDLK_F9);
        CASE(SDLK_F10);
        CASE(SDLK_F11);
        CASE(SDLK_F12);

        CASE(SDLK_PRINTSCREEN);
        CASE(SDLK_SCROLLLOCK);
        CASE(SDLK_PAUSE);
        CASE(SDLK_INSERT);
        CASE(SDLK_HOME);
        CASE(SDLK_PAGEUP);
        CASE(SDLK_DELETE);
        CASE(SDLK_END);
        CASE(SDLK_PAGEDOWN);
        CASE(SDLK_RIGHT);
        CASE(SDLK_LEFT);
        CASE(SDLK_DOWN);
        CASE(SDLK_UP);

        CASE(SDLK_NUMLOCKCLEAR);
        CASE(SDLK_KP_DIVIDE);
        CASE(SDLK_KP_MULTIPLY);
        CASE(SDLK_KP_MINUS);
        CASE(SDLK_KP_PLUS);
        CASE(SDLK_KP_ENTER);
        CASE(SDLK_KP_1);
        CASE(SDLK_KP_2);
        CASE(SDLK_KP_3);
        CASE(SDLK_KP_4);
        CASE(SDLK_KP_5);
        CASE(SDLK_KP_6);
        CASE(SDLK_KP_7);
        CASE(SDLK_KP_8);
        CASE(SDLK_KP_9);
        CASE(SDLK_KP_0);
        CASE(SDLK_KP_PERIOD);

        CASE(SDLK_APPLICATION);
        CASE(SDLK_POWER);
        CASE(SDLK_KP_EQUALS);
        CASE(SDLK_F13);
        CASE(SDLK_F14);
        CASE(SDLK_F15);
        CASE(SDLK_F16);
        CASE(SDLK_F17);
        CASE(SDLK_F18);
        CASE(SDLK_F19);
        CASE(SDLK_F20);
        CASE(SDLK_F21);
        CASE(SDLK_F22);
        CASE(SDLK_F23);
        CASE(SDLK_F24);
        CASE(SDLK_EXECUTE);
        CASE(SDLK_HELP);
        CASE(SDLK_MENU);
        CASE(SDLK_SELECT);
        CASE(SDLK_STOP);
        CASE(SDLK_AGAIN);
        CASE(SDLK_UNDO);
        CASE(SDLK_CUT);
        CASE(SDLK_COPY);
        CASE(SDLK_PASTE);
        CASE(SDLK_FIND);
        CASE(SDLK_MUTE);
        CASE(SDLK_VOLUMEUP);
        CASE(SDLK_VOLUMEDOWN);
        CASE(SDLK_KP_COMMA);
        CASE(SDLK_KP_EQUALSAS400);

        CASE(SDLK_ALTERASE);
        CASE(SDLK_SYSREQ);
        CASE(SDLK_CANCEL);
        CASE(SDLK_CLEAR);
        CASE(SDLK_PRIOR);
        CASE(SDLK_RETURN2);
        CASE(SDLK_SEPARATOR);
        CASE(SDLK_OUT);
        CASE(SDLK_OPER);
        CASE(SDLK_CLEARAGAIN);
        CASE(SDLK_CRSEL);
        CASE(SDLK_EXSEL);

        CASE(SDLK_KP_00);
        CASE(SDLK_KP_000);
        CASE(SDLK_THOUSANDSSEPARATOR);
        CASE(SDLK_DECIMALSEPARATOR);
        CASE(SDLK_CURRENCYUNIT);
        CASE(SDLK_CURRENCYSUBUNIT);
        CASE(SDLK_KP_LEFTPAREN);
        CASE(SDLK_KP_RIGHTPAREN);
        CASE(SDLK_KP_LEFTBRACE);
        CASE(SDLK_KP_RIGHTBRACE);
        CASE(SDLK_KP_TAB);
        CASE(SDLK_KP_BACKSPACE);
        CASE(SDLK_KP_A);
        CASE(SDLK_KP_B);
        CASE(SDLK_KP_C);
        CASE(SDLK_KP_D);
        CASE(SDLK_KP_E);
        CASE(SDLK_KP_F);
        CASE(SDLK_KP_XOR);
        CASE(SDLK_KP_POWER);
        CASE(SDLK_KP_PERCENT);
        CASE(SDLK_KP_LESS);
        CASE(SDLK_KP_GREATER);
        CASE(SDLK_KP_AMPERSAND);
        CASE(SDLK_KP_DBLAMPERSAND);
        CASE(SDLK_KP_VERTICALBAR);
        CASE(SDLK_KP_DBLVERTICALBAR);
        CASE(SDLK_KP_COLON);
        CASE(SDLK_KP_HASH);
        CASE(SDLK_KP_SPACE);
        CASE(SDLK_KP_AT);
        CASE(SDLK_KP_EXCLAM);
        CASE(SDLK_KP_MEMSTORE);
        CASE(SDLK_KP_MEMRECALL);
        CASE(SDLK_KP_MEMCLEAR);
        CASE(SDLK_KP_MEMADD);
        CASE(SDLK_KP_MEMSUBTRACT);
        CASE(SDLK_KP_MEMMULTIPLY);
        CASE(SDLK_KP_MEMDIVIDE);
        CASE(SDLK_KP_PLUSMINUS);
        CASE(SDLK_KP_CLEAR);
        CASE(SDLK_KP_CLEARENTRY);
        CASE(SDLK_KP_BINARY);
        CASE(SDLK_KP_OCTAL);
        CASE(SDLK_KP_DECIMAL);
        CASE(SDLK_KP_HEXADECIMAL);

        CASE(SDLK_LCTRL);
        CASE(SDLK_LSHIFT);
        CASE(SDLK_LALT);
        CASE(SDLK_LGUI);
        CASE(SDLK_RCTRL);
        CASE(SDLK_RSHIFT);
        CASE(SDLK_RALT);
        CASE(SDLK_RGUI);

        CASE(SDLK_MODE);

        CASE(SDLK_AUDIONEXT);
        CASE(SDLK_AUDIOPREV);
        CASE(SDLK_AUDIOSTOP);
        CASE(SDLK_AUDIOPLAY);
        CASE(SDLK_AUDIOMUTE);
        CASE(SDLK_MEDIASELECT);
        CASE(SDLK_WWW);
        CASE(SDLK_MAIL);
        CASE(SDLK_CALCULATOR);
        CASE(SDLK_COMPUTER);
        CASE(SDLK_AC_SEARCH);
        CASE(SDLK_AC_HOME);
        CASE(SDLK_AC_BACK);
        CASE(SDLK_AC_FORWARD);
        CASE(SDLK_AC_STOP);
        CASE(SDLK_AC_REFRESH);
        CASE(SDLK_AC_BOOKMARKS);

        CASE(SDLK_BRIGHTNESSDOWN);
        CASE(SDLK_BRIGHTNESSUP);
        CASE(SDLK_DISPLAYSWITCH);
        CASE(SDLK_KBDILLUMTOGGLE);
        CASE(SDLK_KBDILLUMDOWN);
        CASE(SDLK_KBDILLUMUP);
        CASE(SDLK_EJECT);
        CASE(SDLK_SLEEP);
        CASE(SDLK_APP1);
        CASE(SDLK_APP2);

        CASE(SDLK_AUDIOREWIND);
        CASE(SDLK_AUDIOFASTFORWARD);

    default:
        return "NULL";
        break;
    }
}
